/*
Dada la calificación aprobatoria de la UAC, determine si el alumno Aprobó
(7-10), en caso contrario imprimir No aprobatoria.
 */
package aprobadoreprobado;
import java.util.Scanner;
/**
 *
 * @author juanm
 */
public class AprobadoReprobado {

    static float calif;
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Ingresar la calificacion:");
        calif = sc.nextFloat();
        if (calif >= 7){
            System.out.println("Aprobatoria");
        }else{
            System.out.println("No aprobatoria");
        }
    }
    
}
